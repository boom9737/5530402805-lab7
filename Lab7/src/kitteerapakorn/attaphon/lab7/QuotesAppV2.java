package kitteerapakorn.attaphon.lab7;

import javax.swing.*;

import java.awt.*;

public class QuotesAppV2 extends QuotesAppV1 {
	private JTextArea text;
	private Font fontText;
	private JRadioButton btLgThai, btLgEng;
	private JCheckBox wisdom, technology, society, politics;
	private JComboBox numQuotes;
	private JList listAuthors;
	private JButton btSubmit, btCancel;

	QuotesAppV2(String title) {
		super(title); // �觤����ѧ�͹ʵ�Ѥ�������
		JLabel Img = new JLabel(new ImageIcon(this.getClass().getResource(""))); // �������ٻŧ�
																					// label
																					// Img
		window.add(Img); // ��� Img ŧ� window ���������ٻ
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				QuotesAppV2 qa = new QuotesAppV2("Quotes App V2"); // ���ҧ
																	// object qa
																	// ����
																	// Quotes
																	// App V2
				qa.addComponents(); // ���¡�� Method addComponents
				qa.addMenus(); // ���¡�� Method addMenus
			}
		});
	}

	public void addComponents() {
		JPanel pnCenter = new JPanel(); // ���ҧ Panel
		pnCenter.setLayout(new GridLayout(1, 1)); // ��駤�� Layout
		window.add(new JLabel("Current Quote:"), BorderLayout.NORTH); // ���
																		// Label
																		// ŧ��
																		// window
																		// ��ǹ��
		text = new JTextArea(
				"�������Ҿ�ǧ�� ʹյ��ҹ����� ���Ш����ҧ �ѹ���ҹ����� �Ѻ��ͧ����� ͹Ҥ������֧�ѡ�� �֧��������������繻Ѩ�غѹ ��觷���Դ��͹��������� ��觷��Դ��Ҩ��Դ��� ��ǹ�����Ҿ�ǧ�� ������Ƿ���դ������� ������Ƿ���繨�ԧ ��ͻѨ�غѹ�͹����ͧ \n\n�������ͷء��觷�����Թ �ҡ���Թ����ͧ���ҧ����ͧ�ͧ����� ���Թ��仡Ѻ�� ���ҡ���Թ�ҧ����ͧ������� �ͧ����� ����º������ �ѧࡵ��ó� ���¹��������ԧ���µ���ͧ ���ٴ�֧�����蹷�����������˹�� �������͹��� �ǡ�ҡ��ѧ��觿ѧ�������ͧ���ǡѺ������� ���������·�ȹ� ����þԨ�ó�����ͨС���Ƕ֧�ؤ�ŷ����� \n\n-Achara Klinsuwan",
				10, 15); // ���ҧ textArea
		text.setForeground(Color.BLUE); // ��駤���յ��˹ѧ���
		fontText = new Font("Serif", Font.PLAIN, 16); // ���ҧ object
														// ���ѡɳТͧ
														// Font
		text.setFont(fontText); // set font
		text.setLineWrap(true); // set ����鹺�÷Ѵ�����ѵ��ѵ�
		text.setWrapStyleWord(true); // set ���Ѵ�繤ӵ͹��鹺�÷Ѵ����
		JScrollPane scbText = new JScrollPane(text); // ���ҧ Scroll Bar �ͧ
														// Text
		pnCenter.add(scbText); // ���ŧ� Panel
		window.add(pnCenter, BorderLayout.CENTER); // ��� Panel pnCenter
													// ŧ���ǹ��ҧ�ͧ window

		JPanel pnSouth = new JPanel(); // ���ҧ�����红�������ǹ��ҧ �ͧ
										// window
		pnSouth.setLayout(new BoxLayout(pnSouth, BoxLayout.Y_AXIS)); // set ���
																		// Panel
																		// ��Ẻ
																		// BoxLayout
																		// ����ǵ��

		JPanel pnLanguage = new JPanel(); // ���ҧ Panel ����ǹ�ͧ Language
		pnLanguage.setLayout(new GridLayout(1, 2)); // ��駤�� Layout
		pnLanguage.add(new JLabel("Language:")); // ��� ��ͤ���ŧ�
		JPanel pnBtLanguage = new JPanel(); // ���ҧ Panel ����ǹ�ͧ Button
		pnBtLanguage.setLayout(new FlowLayout(FlowLayout.CENTER)); // ��駤��
																	// Layout
		ButtonGroup lgGroup = new ButtonGroup(); // �ӡ�����ͧ RadioButton
		btLgThai = new JRadioButton("Thia", true); // ���ҧ����
													// RadioButton
		btLgEng = new JRadioButton("English"); // ���ҧ����
												// RadioButton
		lgGroup.add(btLgThai); // ��� RadioButton ŧ㹡����
		lgGroup.add(btLgEng); // ��� RadioButton ŧ㹡����
		pnBtLanguage.add(btLgThai); // ��� RadioButton ŧ� Panel
		pnBtLanguage.add(btLgEng); // ��� RadioButton ŧ� Panel
		pnLanguage.add(pnBtLanguage); // ��� Panel ���� ŧ Panel pnLanguage
		pnSouth.add(pnLanguage); // ��� Panel ŧ� Panel �����ǹ��ҧ

		JPanel pnType = new JPanel(); // ���ҧ Panel ����ǹ�ͧ Type
		pnType.setLayout(new GridLayout(1, 2)); // ��駤�� Layout
		pnType.add(new JLabel("Types:")); // ��� ��ͤ���ŧ�
		JPanel pnBtType = new JPanel(); // ���ҧ Panel ����ǹ�ͧ Button
		pnBtType.setLayout(new FlowLayout(FlowLayout.CENTER)); // ��駤�� Layout
		wisdom = new JCheckBox("Wisdom", true); // ���ҧ���� CkeckBox
		technology = new JCheckBox("Technology"); // ���ҧ���� CkeckBox
		society = new JCheckBox("Society"); // ���ҧ���� CkeckBox
		politics = new JCheckBox("Politics"); // ���ҧ���� CkeckBox
		pnBtType.add(wisdom); // ������ CheckBox ŧ� Panel �������
		pnBtType.add(technology); // ������ CheckBox ŧ� Panel �������
		pnBtType.add(society); // ������ CheckBox ŧ� Panel �������
		pnBtType.add(politics); // ������ CheckBox ŧ� Panel �������
		pnType.add(pnBtType); // ��� Panel ������� ŧ� Panel pnType
		pnSouth.add(pnType); // ��� Panel pnType ŧ� Panel �����ǹ��ҧ

		JPanel pnNumQuotes = new JPanel(); // ���ҧ Panel ����ǹ�ͧ num of
											// Qoutes
		pnNumQuotes.setLayout(new GridLayout(1, 2)); // ��駤�� Layout
		pnNumQuotes.add(new JLabel("Number of Quotes:")); // ��� ��ͤ���ŧ�
		String numQ[] = { "1", "2", "3" }; // ���� ��¡��
		numQuotes = new JComboBox(numQ); // ���ҧ object comboBox
		pnNumQuotes.add(numQuotes); // ��� ComboBox ŧ� Panel pnNumQuotes
		pnSouth.add(pnNumQuotes); // ��� Panel pnNumQuotes ŧ� Panel
									// �����ǹ��ҧ

		JPanel pnAuthors = new JPanel(); // ���ҧ Panel ����ǹ�ͧ Anuthors
		pnAuthors.setLayout(new GridLayout(1, 2)); // ��駤�� Layout
		pnAuthors.add(new JLabel("Authors:")); // ��� ��ͤ���ŧ�
		String objList[] = { "Achara Klinsuwan", "Buddha", "Dungtrin",
				"Earl Nightingale", "Einstein", "Ghandi", "Phra Paisal Visalo",
				"V.Vajiramedhi" }; // ���� List ��ҧ�
		listAuthors = new JList(objList); // ���ҧ object ����� List
		pnAuthors.add(listAuthors); // ��� List ŧ� Panel pnAuthors
		pnSouth.add(pnAuthors); // ��� Panel pnAuthors ŧ� Panel �����ǹ��ҧ

		JPanel submitAndCancel = new JPanel(); // ���ҧ Panel ����ǹ�ͧ����
		submitAndCancel.setLayout(new FlowLayout(FlowLayout.CENTER)); // ��駤��
																		// Layout
		btSubmit = new JButton("Submit"); // ���ҧ����
		btCancel = new JButton("Cancel"); // ���ҧ����
		submitAndCancel.add(btSubmit); // ������ŧ� Panel
		submitAndCancel.add(btCancel); // ������ŧ� Panel
		pnSouth.add(submitAndCancel); // ��� Panel pnType ŧ� Panel �����ǹ��ҧ

		window.add(pnSouth, BorderLayout.SOUTH); // ��� Panel �����ǹ��ҧ ŧ�
													// ��ǹ��ҧ�ͧ window

	}
}
