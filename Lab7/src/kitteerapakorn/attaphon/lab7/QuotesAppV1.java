package kitteerapakorn.attaphon.lab7;

import javax.swing.*;

public class QuotesAppV1 {
	private JMenuBar menuBar; // ���ҧ ����������ٺ���
	protected JFrame window; // ���ҧ ����������
	private JLabel Img; // ���ҧ ��������ٻ

	// ���ҧ ������� MenuItem ��ҧ�
	private JMenuItem fileItemNew;
	private JMenuItem fileItemOpen;
	private JMenuItem fileItemSave;
	private JMenuItem fileItemExit;
	private JMenuItem setColorItemRed;
	private JMenuItem setColorItemGreen;
	private JMenuItem setColorItemBlue;
	private JMenuItem sizeItem16;
	private JMenuItem sizeItem18;
	private JMenuItem sizeItem20;
	private JMenuItem styleBold;
	private JMenuItem styleItalic;
	private JMenuItem styleBoldItalic;

	QuotesAppV1(String title) { // �͹ʵ�Ѥ����
		window = new JFrame(title); // ��˹���� window
									// ���������ժ��͵������Ѻ�����
		window.setSize(500, 650); // ��˹���Ҵ�ͧ window
		window.setLocation(100, 100);// ��˹����˹觢ͧ window
		window.setVisible(true); // �ʴ� window
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // �����������ͻԴ
		// �ա 1 �Ը� Img = new JLabel(new ImageIcon("bin/time.jpeg"));
		Img = new JLabel(new ImageIcon(this.getClass().getResource(
				"/time.jpeg"))); // ����ٻŧ� label Img
		window.add(Img); // ��� Img ŧ� window

	}

	public void addMenus() { // ���ҧ����
		JMenu fileMenu = new JMenu("File"); // ���ҧ���� File
		menuBar = new JMenuBar(); // ���ҧ MenuBar
		fileItemNew = new JMenuItem("New"); // ���ҧ MenuItem
		fileItemOpen = new JMenuItem("Open", new ImageIcon(this.getClass()
				.getResource("/OpenIcon.png"))); // ���ҧ���� Item ��������Icon
		fileItemSave = new JMenuItem("Save"); // ���ҧ MenuItem
		fileItemExit = new JMenuItem("Exit"); // ���ҧ MenuItem
		fileMenu.add(fileItemNew); // ��� MenuItem ŧ� Menu File
		fileMenu.add(fileItemOpen); // ��� MenuItem ŧ� Menu File
		fileMenu.add(fileItemSave); // ��� MenuItem ŧ� Menu File
		fileMenu.add(fileItemExit); // ��� MenuItem ŧ� Menu File
		JMenu editMenu = new JMenu("Edit"); // ���ҧ���� Edit
		JMenu editItemSetColor = new JMenu("Set Color"); // ���ҧ���� Set color
		setColorItemRed = new JMenuItem("Red"); // ���ҧ MenuItem
		setColorItemGreen = new JMenuItem("Green"); // ���ҧ MenuItem
		setColorItemBlue = new JMenuItem("Blue"); // ���ҧ MenuItem
		editItemSetColor.add(setColorItemRed); // ��� MenuItem ŧ� Menu Set
												// Color
		editItemSetColor.add(setColorItemGreen); // ��� MenuItem ŧ� Menu Set
													// Color
		editItemSetColor.add(setColorItemBlue); // ��� MenuItem ŧ� Menu Set
												// Color
		JMenu editItemSetFont = new JMenu("Set Font"); // ���ҧ���� Set Font
		JMenu setFontSize = new JMenu("Size"); // ���ҧ���� Set Size
		sizeItem16 = new JMenuItem("16"); // ���ҧ MenuItem
		sizeItem18 = new JMenuItem("18"); // ���ҧ MenuItem
		sizeItem20 = new JMenuItem("20"); // ���ҧ MenuItem
		setFontSize.add(sizeItem16); // ��� MenuItem ŧ� Menu Set Size
		setFontSize.add(sizeItem18); // ��� MenuItem ŧ� Menu Set Size
		setFontSize.add(sizeItem20); // ��� MenuItem ŧ� Menu Set Size
		JMenu setFontStyle = new JMenu("Style"); // ���ҧ���� Style
		styleBold = new JMenuItem("Bold"); // ���ҧ MenuItem
		styleItalic = new JMenuItem("Italic"); // ���ҧ MenuItem
		styleBoldItalic = new JMenuItem("Bold Italic"); // ���ҧ MenuItem
		setFontStyle.add(styleBold); // ��� MenuItem ŧ� Menu Style
		setFontStyle.add(styleItalic); // ��� MenuItem ŧ� Menu Style
		setFontStyle.add(styleBoldItalic); // ��� MenuItem ŧ� Menu Style
		editItemSetFont.add(setFontSize); // ��� Menu Size ŧ� Menu Set Font
		editItemSetFont.add(setFontStyle); // ��� Menu Style ŧ� Menu Set Font

		editMenu.add(editItemSetColor); // ��� Menu Set color ŧ� Menu Edit
		editMenu.add(editItemSetFont); // ��� Menu Set Font ŧ� Menu Edit

		menuBar.add(fileMenu); // ��� Menu File ŧ� MenuBar
		menuBar.add(editMenu); // ��� Menu Edit ŧ� MenuBar

		window.setJMenuBar(menuBar); // ��� MenuBar ŧ� window
		window.pack(); // �Ѵ���§��觷������� window �ѵ��ѵ�
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				QuotesAppV1 qa = new QuotesAppV1("Quotes App V1"); // ���ҧ
																	// object qa
																	// �¡�˹����ͤ��
																	// "Quotes App V1"
				qa.addMenus(); // ���¡�� addMenus �ͧ qa
			}
		});
	}
}
